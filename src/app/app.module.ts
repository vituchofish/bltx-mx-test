import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app.routing';

import { FullLayoutComponent } from './layout/full-layout/full-layout.component';
import { RestService } from '@app-services/';
import { GenericRestService, StorageService, STORAGE_PREFIX } from '@app-support-services/';
import { ExchangerService } from './core/services/exchanger.service';

// aquí hay un ejemplo ...

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
		FormsModule,
    BrowserAnimationsModule,
		ReactiveFormsModule,
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
  ],
  providers: [
		RestService,
    GenericRestService,
    StorageService,
    ExchangerService,
    { provide: STORAGE_PREFIX, useValue: 'MoXch' },
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
