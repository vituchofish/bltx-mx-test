import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';
import { Subject } from 'rxjs';
import 'rxjs/Rx';

import { ExchangerService } from '@app/core/services/exchanger.service';

@Component({
  templateUrl: 'exchange.component.html'
})
export class ExchangeComponent implements OnInit, OnDestroy {
  form: FormGroup;
  public isLoading: boolean = false;
  private destroy$: Subject<void> = new Subject<void>();
  private currencyTo: string = 'USD';
  private currency: CurrencyPipe = new CurrencyPipe('en-US');
  private format: string = '1.1-4';
  public currencyMaskOptions = { prefix: '€', thousands: ',', decimal: '.', precision: 4 };

  constructor(
    private readonly fb: FormBuilder,
    private readonly exchangerService: ExchangerService
  ) {
    this.exchangerService.base = 'EUR';    
    this.createForm();
  };

  ngOnInit() {
  };

  private createForm(): void {
    this.form = this.fb.group({
      Amount: [ '', Validators.compose([ Validators.required ]) ],
      ResultAmount: ['']
    });
  };

  calculate() {
    if (this.form.valid) {
      this.isLoading = true;
      this.exchangerService.getRate(this.currencyTo)
        .takeUntil(this.destroy$)
        .subscribe((response: { success: boolean, data: number }) => {
          if (response.success) {
            this.performCalculation(response.data);
          }
          this.isLoading = false;
        }, error => {
          this.isLoading = false;
          console.error(error);
        });
    }
  };

  performCalculation(rate: number) {
    let amount = this.form.get('Amount').value;
    this.form.get('ResultAmount').reset();
    amount = parseFloat(amount);
    if (!isNaN(amount)) {
      rate = parseFloat(rate.toString());
      if (!isNaN(rate)) {
        let value = (amount * rate).toString();
        value = this.currency.transform(value, this.currencyTo, 'symbol', this.format);
        this.form.patchValue({ 'ResultAmount': value });
        this.form.get('ResultAmount').markAsDirty();
      }
    }
  };

  ngOnDestroy() {
    this.exchangerService.clear();
		this.destroy$.next();
    this.destroy$.complete();
	};
} 