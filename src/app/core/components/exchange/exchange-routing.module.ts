import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExchangeComponent } from '@app/core/components/exchange/exchange.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Exchange'
    },
    children: [
      {
        path: 'exchange',
        component: ExchangeComponent,
        data: {
          title: 'Exchange'
        },
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExchangeRoutingModule {}
