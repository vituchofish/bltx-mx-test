import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExchangeComponent } from '@app/core/components/exchange/exchange.component';
import { ExchangeRoutingModule } from './exchange-routing.module';
import { ErrorViewerComponent } from '@app/support/components';
import { NgxCurrencyModule } from "ngx-currency";
import { DisableControlDirective } from '@app/support/directives';

@NgModule({
  imports: [
    CommonModule,
    ExchangeRoutingModule,
    FormsModule,
    ReactiveFormsModule, 
    NgxCurrencyModule
  ],
  declarations: [ 
    ExchangeComponent,
    ErrorViewerComponent,
    DisableControlDirective,
  ]
})
export class ExchangeModule { }
