import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { StorageService } from '@app/support/services';
import { RestService } from './rest.service';
import { environment } from '@environments/environment';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Rx';

type ExchangeResponse = {
  success: boolean,
  timestamp: number,
  base: string,
  date: string,
  rates: { 
    [key: string]: number 
  }
}

type ExchangeData = {
  timestamp: number,
  response: ExchangeResponse
}

@Injectable()
export class ExchangerService implements OnInit, OnDestroy {

  private isLoading: boolean = false;

  private internalBase: string;
  private intervalId;
  private partExpirationTime: number = 1/3;
  private timeoutForCheckNewExpireTime: number = Math.round(environment.refreshRates * this.partExpirationTime * 1000);

  private destroy$: Subject<any> = new Subject<any>();

	constructor(
      private readonly storage: StorageService
    , private readonly rest: RestService
  ) {
    // this.storage.session.setPrefix('mx');
	};

  ngOnInit() {
    this.clear();
    this.run();
  };

  public set base(base: string) {
    if (base && this.internalBase != base) {
      this.clear();
      this.internalBase = base;
      this.run();
    }
  };

	public getRate(currencyTo: string): Observable<{ success: boolean, data: number }> {
    let rate: number = 0;
    if (this.internalBase) {
      let data: ExchangeData = this.storage.session.get(this.internalBase);
      if (data && data.response && this.isValidExchangeResponse(data.response)) {
        const { response } = data;
        if (response.rates[currencyTo]) rate = response.rates[currencyTo];
      }
      if (rate == 0) {
        return this.rest.get()
          .map((response: ExchangeResponse) => {
            const data: ExchangeData = this.getExchangeData(response);
            this.storage.session.set(this.internalBase, data);
            if (response.success) {
              return { success: response.success, data: response.rates[currencyTo] };
            } else {
              return { success: response.success, data: 0 };
            }
          })
          .switchMap((response: { success: boolean, data: number }) => {
            return Observable.of({ success: response.success, data: response.data });
          });
      } else {
        return Observable.of({ success: true, data: rate });
      }
    } else {
      return Observable.of({ success: false, data: 0 });
    }
  };

  private run() {
    if (!this.intervalId) this.performLoad();
    this.intervalId = setInterval(() => {
      this.performLoad();
    }, this.timeoutForCheckNewExpireTime);
  };

  private performLoad() {
    if (this.mustLoad()) {
      this.isLoading = true;
      this.rest.get()
        .takeUntil(this.destroy$)
        .subscribe((response: ExchangeResponse) => {
          if (this.internalBase) {
            const data: ExchangeData = this.getExchangeData(response);
            this.storage.session.set(this.internalBase, data);
            this.isLoading = false;
          }
        },error => {
          this.isLoading = false;
          console.error('.: Error :.', error);
        });
    }
  };

  public getExchangeData(response: ExchangeResponse): ExchangeData {
    return { timestamp: this.getCurrentTime(), response };
  };

  private mustLoad() {
    let data: ExchangeData = this.storage.session.get(this.internalBase);
    let load: boolean = true;
    if (data) {
      const { response } = data;
      if (response && response.success && this.isValidExchangeResponse(response)) {
        let expired: boolean = this.checkTimeout(data.timestamp);
        if (expired) this.clear();
        load = expired;
      }
    }
    return load;
  };

  private checkTimeout(timestampLastLoad: number) {
    let timestampCurrent: number = this.getCurrentTime();
    return (timestampCurrent - timestampLastLoad) >= environment.refreshRates;
  };

  private isValidExchangeResponse(response: ExchangeResponse) {
    if (!response) return false;
    return Object.keys(response).includes('base') && Object.keys(response).includes('timestamp') && Object.keys(response).includes('rates');
  };

  private getCurrentTime() : number {
    return Math.round((new Date()).getTime()/1000);
  };
  
  public clear() {
    if (this.intervalId > 0) clearInterval(this.intervalId);
    this.storage.session.removeFromPrefix();
  };

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
    this.clear();
  };

}