import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { environment } from '@environments/environment';

import { GenericRestService } from '@app-support-services/';

@Injectable()
export class RestService {

	private actionUrl: string;

	constructor(private http: GenericRestService) {
		this.actionUrl = environment.urlApi;
	}

	get(): Observable<any> {
		return this.http.get<any>(`${this.actionUrl}?access_key=${environment.accessKey}`)
			.catch(this.handleError);
	}

	private handleError(error: HttpResponse<any>) {
		return Observable.throw(error || 'Server error');
	}

}    