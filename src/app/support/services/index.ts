export { GenericRestService } from './generic-rest.service';
export { StorageService, STORAGE_PREFIX } from './storage.service';