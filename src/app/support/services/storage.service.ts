import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';

enum StorageType {
  Local,
  Session
}

class StorageHandler {
  storage: Storage;

  constructor(type: StorageType, prefix?: string) { 
    this.prefix = prefix || this.prefix;
    switch(type) {
      case StorageType.Local:
        this.storage = localStorage;
        this.prefix = `${this.prefix}-ls`;
        break;
      case StorageType.Session:
      default:
        this.storage = sessionStorage;
        this.prefix = `${this.prefix}-ss`;
        break;
    }
  }

  private prefix: string = 'ng';
  private separator: string = '|';
  private typeSeparator: string = '$$';

  private setValidName(name: string) {
    return name.replace(/\//g, '-');
  };
  private getListKeyNames = (prefix?: string) => {
    let list: Array<string> = [];
    for (let i = 0; i < this.storage.length; i++) {
      if (prefix) {
        if (this.storage.key(i).startsWith(prefix)) {
          list.push(this.storage.key(i));
        }
      } else {
        list.push(this.storage.key(i));
      }
    }
    return list;
  };
  // public setPrefix(prefix: string) {
  //   this.prefix = prefix;
  // };      
  public set(key: string, data: any): void {
    try {
      key = this.prefix + this.separator + this.setValidName(key);
      let value: string = data;
      let type: string = typeof data;
      if (typeof data === 'object') {
        value = JSON.stringify(data);
        if (data instanceof Date) { type = 'date'; }
      } else {  // 'string' | 'number' | 'boolean' | 'function' | 'undefined'
        value = data as string;
      }
      value += this.typeSeparator + type;
      this.storage.setItem(key, btoa(value));
    } catch (error) {
      console.error(error);
    }
  };
  public get(key: string): any {
    let data: any;
    try {
      key = this.prefix + this.separator + this.setValidName(key);
      let value: string = this.storage.getItem(key);
      if (value) {
        value = atob(value);
        if (value) {
          let [target, type] = value.split(this.typeSeparator);
          switch(type) {
            case 'object':
              data = JSON.parse(target);
              break;
            case 'date':
              data = new Date(JSON.parse(target));
              break;
            case 'string':
              data = target as string;
              break;
            case 'number':
              data = +target;
              break;
            case 'boolean':
              data = !!target;
              break;
            case 'function':
              data = new Function(target);
              break;
            case 'undefined':
              data = target as undefined;
              break;
            default:
              data = target;
              break;
          }
        }
      }
    } catch(error) {
      console.error(error);
    }
    return data;
  };
  public remove(key: string): void {
    key = this.prefix + this.separator + this.setValidName(key);
    this.storage.removeItem(key);
  };
  public removeFromPrefix(prefix?: string): void {
    prefix = prefix || this.prefix;
    prefix = prefix + this.separator;
    const list: Array<string> = this.getListKeyNames(prefix);
    for (let i = 0; i < list.length; i++) {
      this.storage.removeItem(list[i]);
    }
  };
  public clear(): void {
    this.storage.clear();
  };
  public key(index: number): string {
    return this.storage.key(index);
  };
  public get length(): number {
    return this.storage.length;
  };
  public getPrefix(): string {
    return this.prefix;
  };
}

export const STORAGE_PREFIX = new InjectionToken<string>('storage-prefix.config');

@Injectable()
export class StorageService {
  constructor(
    @Inject(STORAGE_PREFIX) @Optional() public prefix?: string
  ) { }

	public get local() {
    return new StorageHandler(StorageType.Local, this.prefix);
  }

	public get session() {
    return new StorageHandler(StorageType.Session, this.prefix);
  }

}