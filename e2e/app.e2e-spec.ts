import { TestBed, async } from '@angular/core/testing';
import { StorageService, GenericRestService, STORAGE_PREFIX } from "@app/support/services";
import { ExchangerService } from "@app/core/services/exchanger.service";
import { RestService } from "@app/core/services";

describe('Money Xchange Test', () => {
  let service: ExchangerService;
  let currencyFrom: string = 'EUR';
  let currencyTo: string = 'USD';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        RestService,
        GenericRestService,
        StorageService,
        ExchangerService,
        { provide: STORAGE_PREFIX, useValue: 'MoXch' },
      ]
    }).compileComponents()
    .then(() => {
      service = TestBed.get(ExchangerService);
      service.base = currencyFrom;
    });

  }));

  it('Test de la funcionalidad del servicio que obtiene los tipos de cambio.', async () => {
    service.getRate(currencyTo)
      .subscribe(
        response => {
          let { data } = response;
          expect(data).toBeGreaterThanOrEqual(0);
          expect(data).toBeDefined();
        }
      );
  });
});